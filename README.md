# Ti-Basic TI80



## introduction

this repository contains some programmes, i've create during my studies.
it was programmed in TI-Basic language (for Texas Instrument TI80 models)

## the programmes:

- [ ] [distance] calcul the distance between two points using their coordinates
- [ ] [Fraisage] calcul the milling parameters "VC" and "N"
- [ ] [FraisageV2] calcul the milling parameters "VC" and "N" with a graphical interfaces
- [ ] [Tournage] calcul the turning parameters "VC" and "N"
- [ ] [TournageV2] calcul the turning parameters "VC" and "N" with a graphical interfaces
